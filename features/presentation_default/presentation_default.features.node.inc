<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _presentation_default_node_info() {
  $items = array(
    'presentation' => array(
      'name' => t('Presentation'),
      'module' => 'features',
      'description' => t('A <em>presentation</em> is content specifically intended to be shown through a specially slideshow mode.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => t('Every HTML heading defines a new slide. Text before any HTML heading is excluded from the presentation.'),
    ),
  );
  return $items;
}
