<?php

/**
 * Helper to implementation of hook_ctools_plugin_api().
 */
function _presentation_default_ctools_plugin_api() {
  $args = func_get_args();
  $module = array_shift($args);
  $api = array_shift($args);
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Helper to implementation of hook_strongarm().
 */
function _presentation_default_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_presentation';
  $strongarm->value = 0;

  $export['comment_anonymous_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_presentation';
  $strongarm->value = '3';

  $export['comment_controls_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_presentation';
  $strongarm->value = '4';

  $export['comment_default_mode_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_presentation';
  $strongarm->value = '1';

  $export['comment_default_order_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_presentation';
  $strongarm->value = '50';

  $export['comment_default_per_page_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_presentation';
  $strongarm->value = '0';

  $export['comment_form_location_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_presentation';
  $strongarm->value = '2';

  $export['comment_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_presentation';
  $strongarm->value = '1';

  $export['comment_preview_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_presentation';
  $strongarm->value = '1';

  $export['comment_subject_field_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_presentation';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );

  $export['node_options_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'presentation_allowed_node_types';
  $strongarm->value = array(
    'presentation' => 'presentation',
    'book' => 0,
    'casetracker_basic_case' => 0,
    'blog' => 0,
    'event' => 0,
    'group' => 0,
    'profile' => 0,
    'casetracker_basic_project' => 0,
    'shoutbox' => 0,
    'project_ticket' => 0,
    'feed_ical_item' => 0,
    'feed_ical' => 0,
  );

  $export['presentation_allowed_node_types'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'presentation_default_arguments';
  $strongarm->value = array(
    'title' => '[title]',
    'author' => '[author-name]',
    'email' => '[author-mail]',
    'date' => '[day], [mon] [date], [yyyy]',
    'organization' => '[ogname]',
    'location' => '[site-name]',
  );

  $export['presentation_default_arguments'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'presentation_default_plugin';
  $strongarm->value = 'handout';

  $export['presentation_default_plugin'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'presentation_defaults';
  $strongarm->value = array(
    'title' => '[title]',
    'author' => '[author-name]',
    'email' => '[author-mail]',
    'date' => '[day], [mon] [date], [yyyy]',
    'company' => '[ogname]',
    'location' => '[site-name]',
  );

  $export['presentation_defaults'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'presentation_node_link_text';
  $strongarm->value = 'View Presentation';

  $export['presentation_node_link_text'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'presentation_s5_settings';
  $strongarm->value = array(
    'theme' => 'default',
  );

  $export['presentation_s5_settings'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'presentation_slide_source';
  $strongarm->value = array(
    'type' => 'node',
    'content' => '[node-body]',
  );

  $export['presentation_slide_source'] = $strongarm;
  return $export;
}
