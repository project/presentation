<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function presentation_default_ctools_plugin_api() {
  module_load_include('inc', 'presentation_default', 'presentation_default.defaults');
  $args = func_get_args();
  return call_user_func_array('_presentation_default_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_node_info().
 */
function presentation_default_node_info() {
  module_load_include('inc', 'presentation_default', 'presentation_default.features.node');
  $args = func_get_args();
  return call_user_func_array('_presentation_default_node_info', $args);
}

/**
 * Implementation of hook_strongarm().
 */
function presentation_default_strongarm() {
  module_load_include('inc', 'presentation_default', 'presentation_default.defaults');
  $args = func_get_args();
  return call_user_func_array('_presentation_default_strongarm', $args);
}
