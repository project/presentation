<?php

/**
 * Helper to implementation of hook_context_default_contexts().
 */
function _presentation_atrium_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feature-presentation';
  $context->description = '';
  $context->tag = 'Presentation';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'presentation' => 'presentation',
      ),
      'options' => array(
        'node_form' => 1,
      ),
    ),
    'presentation_node' => array(
      'values' => array(
        1 => 1,
      ),
      'options' => array(
        'node_form' => 1,
      ),
    ),
    'views' => array(
      'values' => array(
        'presentation_listing' => 'presentation_listing',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'presentations',
  );
  $context->condition_mode = 0;

  $export['feature-presentation'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feature-presentation-node';
  $context->description = '';
  $context->tag = 'Presentation';
  $context->conditions = array(
    'presentation_node' => array(
      'values' => array(
        1 => 1,
      ),
      'options' => array(
        'node_form' => 0,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'presentation-presentation_types' => array(
          'module' => 'presentation',
          'delta' => 'presentation_types',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  $export['feature-presentation-node'] = $context;
  return $export;
}

/**
 * Helper to implementation of hook_ctools_plugin_api().
 */
function _presentation_atrium_ctools_plugin_api() {
  $args = func_get_args();
  $module = array_shift($args);
  $api = array_shift($args);
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Helper to implementation of hook_user_default_permissions().
 */
function _presentation_atrium_user_default_permissions() {
  $permissions = array();

  // Exported permission: create presentation content
  $permissions[] = array(
    'name' => 'create presentation content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: delete any presentation content
  $permissions[] = array(
    'name' => 'delete any presentation content',
    'roles' => array(
      '0' => 'manager',
    ),
  );

  // Exported permission: delete own presentation content
  $permissions[] = array(
    'name' => 'delete own presentation content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit any presentation content
  $permissions[] = array(
    'name' => 'edit any presentation content',
    'roles' => array(
      '0' => 'manager',
    ),
  );

  // Exported permission: edit own presentation content
  $permissions[] = array(
    'name' => 'edit own presentation content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  return $permissions;
}

/**
 * Helper to implementation of hook_strongarm().
 */
function _presentation_atrium_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'atrium_update_type_presentation';
  $strongarm->value = 1;

  $export['atrium_update_type_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_presentation';
  $strongarm->value = 1;

  $export['enable_revisions_page_presentation'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'og_content_type_usage_presentation';
  $strongarm->value = 'group_post_wiki';

  $export['og_content_type_usage_presentation'] = $strongarm;
  return $export;
}
