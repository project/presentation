<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function presentation_atrium_context_default_contexts() {
  module_load_include('inc', 'presentation_atrium', 'presentation_atrium.defaults');
  $args = func_get_args();
  return call_user_func_array('_presentation_atrium_context_default_contexts', $args);
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function presentation_atrium_ctools_plugin_api() {
  module_load_include('inc', 'presentation_atrium', 'presentation_atrium.defaults');
  $args = func_get_args();
  return call_user_func_array('_presentation_atrium_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_user_default_permissions().
 */
function presentation_atrium_user_default_permissions() {
  module_load_include('inc', 'presentation_atrium', 'presentation_atrium.defaults');
  $args = func_get_args();
  return call_user_func_array('_presentation_atrium_user_default_permissions', $args);
}

/**
 * Implementation of hook_strongarm().
 */
function presentation_atrium_strongarm() {
  module_load_include('inc', 'presentation_atrium', 'presentation_atrium.defaults');
  $args = func_get_args();
  return call_user_func_array('_presentation_atrium_strongarm', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function presentation_atrium_views_default_views() {
  module_load_include('inc', 'presentation_atrium', 'presentation_atrium.features.views');
  $args = func_get_args();
  return call_user_func_array('_presentation_atrium_views_default_views', $args);
}
