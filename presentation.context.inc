<?php

/**
 * @file
 *  Describes context plugins for the Presentation module.
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function presentation_ctools_plugin_api($module, $api) {
  if ($module == 'context' && $api == 'plugins') {
    return array('version' => 3);
  }
}

/**
 * Implementation of hook_context_registry().
 */
function presentation_context_registry() {
  $registry = array();
  $registry['conditions'] = array(
    'presentation_node' => array(
      'title' => t('Presentation node'),
      'description' => t('Set this context when viewing a node page or using the add/edit form of a presentation content type.'),
      'plugin' => 'presentation_condition_node',
    ),
  );
  return $registry;
}

/**
 * Implementation of hook_context_plugins().
 */
function presentation_context_plugins() {
 $plugins = array();
  $plugins['presentation_condition_node'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'presentation') .'/plugins',
      'file' => 'presentation_condition_node.inc',
      'class' => 'presentation_condition_node',
      'parent' => 'context_condition_node',
    ),
  );
  return $plugins;
}

/**
 * Implementation of hook_context_node_condition_alter().
 */
function presentation_context_node_condition_alter($node, $op) {
  if ($plugin = context_get_plugin('condition', 'presentation_node')) {
    $plugin->execute($node, $op);
  }
}