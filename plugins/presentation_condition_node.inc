<?php

/**
 * @file
 *  Context condition testing if the current page is a presentation node type.
 */
class presentation_condition_node extends context_condition_node {
  function condition_values() {
    return array('1' => t('Is a presentation node.'));
  }

  function execute($node, $op) {
    if (!$this->condition_used()) {
      return;
    }
    foreach ($this->get_contexts(1) as $context) {
      if (presentation_is_allowed($node)) {
        // Check the node form option.
        if ($op === 'form') {
          $options = $this->fetch_from_context($context, 'options');
          if (!empty($options['node_form'])) {
            $this->condition_met($context, 1);
          }
        }
        else {
          $this->condition_met($context, 1);
        }
      }
    }      
  }
}