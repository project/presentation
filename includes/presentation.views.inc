<?php

/**
 * Implementation of hook_views_data_alter().
 */
function presentation_views_data_alter(&$data) {
  $data['node']['presentation'] = array(
    'title' => t('Presentation node'),
    'help' => t('Whether the node is enabled for presentations.'),
    'filter' => array(
      'handler' => 'presentation_views_handler_filter_presentation',
    ),
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function presentation_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'presentation') . '/includes',
    ),
    'handlers' => array(
      // Filters: Note-type
      'presentation_views_handler_filter_presentation' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}