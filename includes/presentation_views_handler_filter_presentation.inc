<?php

/**
 * Filter to only show presentation nodes.
 */
class presentation_views_handler_filter_presentation extends views_handler_filter {
  function query() {    
    $presentation_types = array_filter(presentation_get_setting('allowed_node_types'));

    if (!empty($presentation_types)) {
      $placeholders = db_placeholders($presentation_types, 'varchar');
      $table = $this->query->ensure_table('node');
      $this->query->add_where($this->options['group'], "$table.type IN ($placeholders)", $presentation_types);
    }
    else {
      $this->query->add_where($this->options['group'], "FALSE");
      $msg = t('You have no node types which are used as presentations.');
      if (user_access('administer presentations')) {
        $msg .= ' ' . l(t('Configure case tracker.'), 'admin/settings/presentation');
      }
      drupal_set_message($msg, 'warning');
    }
  }
}