<?php

/**
 * @file
 *  Define default plugins and standardized plugin behaviors.
 */

define('PRESENTATION_MAX_HEADER_LEVEL', 6);
define('PRESENTATION_SIMPLE_PLUGIN', 'PRESENTATION_SIMPLE_PLUGIN');

/**
 * Collect all defined display plugins.
 *
 * @param $plugin_id
 *  Array key for a specific plugin. Used to request a single plugin of all available.
 * @param $reset
 *  Boolean. If true will rebuild the plugin definitions.
 *
 * @return
 *  Array of plugins.
 */
function presentation_get_plugins($plugin_id = NULL, $reset = FALSE) {
  static $plugins;

  if ($reset || empty($plugins)) {
    module_load_include('inc', 'presentation', 'presentation.core');
    $plugins = array();
    // Build list of plugins via hook_presentation_plugin_info().
    foreach (module_implements('presentation_plugin_info') as $module) {
      $function = $module . '_' . 'presentation_plugin_info';
      $result = call_user_func($function);
      if (isset($result) && is_array($result)) {
        foreach ($result as $index => $plugin) {
          $result[$index]['module'] = $module;
        }
        $plugins = array_merge($plugins, $result);
      }
    }

    // Impose standard configuration values based on plugin array index.
    // It is more clear if plugins set these explicitly.
    foreach ($plugins as $plugin => $definition) {
      if (empty($definition['label'])) {
        $plugins[$plugin]['label'] = ucfirst($plugin);
      }
      if (empty($definition['machine_name'])) {
        $plugins[$plugin]['machine_name'] = $plugin;
      }
      if (empty($definition['process'])) {
        $plugins[$plugin]['process'] = 'presentation_text_slides_by_heading_process';
      }
      if (empty($definition['template'])) {
        $plugins[$plugin]['template'] = drupal_get_path('module', 'presentation') . '/theme';
      }
      if (empty($definition['library'])) {
        $plugins[$plugin]['library'] = drupal_get_path('module', 'presentation') . '/libraries/' . $plugin;
      }
      if (empty($definition['enabled'])) {
        $plugins[$plugin]['enabled'] = TRUE;
      }
      if (empty($definition['package']['download'])) {
        $plugins[$plugin]['package']['download'] = $definition['package']['homepage'];
      }
    }
    drupal_alter('presentation_plugin_info', $plugins);

    // Make sure all plugins that need a library have one.
    foreach ($plugins as $plugin => $definition) {
      if ($definition['library'] != PRESENTATION_SIMPLE_PLUGIN && !is_dir($definition['library'])) {
        $plugins[$plugin]['enabled'] = FALSE;
      }
    }
  }
  
  if (isset($plugin_id)) {
    return $plugins[$plugin_id];
  }
  return $plugins;
}


// Text Parsing Functions //

/**
 * Process callback for handout plugin.
 * Using straight markup from the node.
 */
function presentation_text_raw_process($text) {
  return array($text);
}

/** 
 * Divide text into individual slides.
 * Slides will be divided by HTML heading.
 *
 * @param $text
 *  Text to be processed.
 * @return
 *  Array of slides, each slide is defined as an array.
 *  E.g., array('title' => 'My First Slide', 'text' => 'My first slide's text');
 */
function presentation_text_slides_by_heading_process($text) {
  if (!preg_match("/<h([1-PRESENTATION_MAX_HEADER_LEVEL])>/i", $text, $match)) {
    if (!empty($text)) {
      // Treat as a single slide.
      return array(array('title' => '', 'text' => $text));
    }
    else {
      // Null body, null slide.
      return array();
    }
  }

  // Build some slides using any HTML header as a slide delimiter.
  $slides = array();
  $text = preg_split("/\<h\d>(.*?)\<\/h\d>/iS", $text, -1, PREG_SPLIT_DELIM_CAPTURE);
  $count = count($text);

  for ($item = 1; $item < $count; $item += 2) {
    $slides[] = array('title' => $text[$item], 'text' => $text[$item+1]);
  }
  return $slides;
}

