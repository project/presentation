<?php

/**
 * @file
 * Admin settings for Presentation module.
 */

/**
 * Admin callback
 * Most of these settings will be moved to a Content Type-based Presentation Profile system.
 */
function presentation_admin_settings() {
  $plugins = presentation_get_plugins();

  // Build an array of options for plugin selection.
  $default_plugin_options = array();
  foreach ($plugins as $plugin) {
    if ($plugin['enabled']) {
      $default_plugin_options[$plugin['machine_name']] = $plugin['label'];
    }
  }

  $form = array();
  $form['presentation_default_plugin'] = array(
    '#type' => 'select',
    '#title' => t('Default Plugin'),
    '#default_value' => presentation_get_setting('default_plugin'),
    '#options' => $default_plugin_options,
  );

  $form['presentation_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Presentation Settings'),
    '#description' => t('Configure node presentations.'),
    '#collapsible' => TRUE,
  );

  // Set content types usable as presentations
  $types = node_get_types('names');
  $form['presentation_node']['presentation_allowed_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed presentation types'),
    '#default_value' => presentation_get_setting('allowed_node_types'),
    '#options' => $types,
    '#description' => t('Select content types to be exportable as presentations'),
    '#required' => TRUE,
  );

  $form['presentation_node']['presentation_node_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Presentation Link Text'),
    '#default_value' => presentation_get_setting('node_link_text'),
  );
  
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#title' => t('Advanced Settings'),
  );

  // Source content for presentation.
  $source = presentation_get_setting('slide_source');
  $form['advanced']['presentation_slide_source'] = array(
    '#type' => 'fieldset',
    '#description' => t('Define the source content for your presentation.'),
    '#tree' => TRUE,
    '#title' => t('Slide Source Content'),
    '#collapsible' => TRUE,
  );

  $form['advanced']['presentation_slide_source']['type'] = array(
    '#type' => 'select',
    '#default_value' => $source['type'],
    '#options' => array('node' => 'node'),
    '#description' => t('Define the type of content source to use.'),
  );

  $form['advanced']['presentation_slide_source']['content'] = array(
    '#type' => 'textarea',
    '#title' => t('Source Content'),
    '#default_value' => $source['content'],
    '#description' => t('Use tokens to define slide content. Each row will be on a separate slide, and may be broken down further depending on the plugin.'),
  );

  // Build-in forms for presentation arguments.
  $form['advanced']['presentation_default_arguments'] = array(
    '#title' => t('Presentation Defaults'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );

  $form['advanced']['presentation_default_arguments']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Presentation Title'),
    '#default_value' => presentation_get_setting('default_arguments', 'title'),
  );

  $form['advanced']['presentation_default_arguments']['author'] = array(
    '#type' => 'textfield',
    '#title' => t('Presentation Author'),
    '#default_value' => presentation_get_setting('default_arguments', 'author'),
  );

  $form['advanced']['presentation_default_arguments']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Presentation Email'),
    '#default_value' => presentation_get_setting('default_arguments', 'email'),
  );

  $form['advanced']['presentation_default_arguments']['date'] = array(
    '#type' => 'textfield',
    '#title' => t('Presentation Date'),
    '#default_value' => presentation_get_setting('default_arguments', 'date'),
  );

  $form['advanced']['presentation_default_arguments']['organization'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization'),
    '#default_value' => presentation_get_setting('default_arguments', 'organization'),
  );

  $form['advanced']['presentation_default_arguments']['location'] = array(
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#default_value' => presentation_get_setting('default_arguments', 'location'),
  );

  $form['advanced']['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['token_help']['help'] = array(
    '#value' => theme('token_help', 'node'),
  );
  
  $libraries = array();
  foreach ($plugins as $plugin) {
    // Append plugin-specific configuration.
    if ($plugin['enabled'] && function_exists($plugin['settings'])) {
      $index = 'presentation_' . $plugin['machine_name'] . '_settings';
      $form[$index] = array(
        '#type' => 'fieldset',
        '#title' => t('!plugin settings', array('!plugin' => $plugin['label'])),
        '#collapsible' => TRUE,
        '#tree' => TRUE,
      );
      $result = call_user_func_array($plugin['settings'], array($plugin));
      foreach ($result as $eid => $element) {
        $form[$index][$eid] = $element;
      }
    }
    if ($plugin['library'] == PRESENTATION_SIMPLE_PLUGIN) {
      continue;
    }
    $version = !empty($plugin['package']['version']) ? ' [Version ' . $plugin['package']['version'] . ']' : '';
  
    $libraries[] = array(l($plugin['label'] . $version, $plugin['package']['homepage'])
      . ' (' . l(t('Download'), $plugin['package']['download']) . ')');

    if (!is_dir($plugin['library'])) {
      $libraries[] = array($plugin['package']['help']);
    }
    else {
      $libraries[] = array();
    }
  }
  // Collect info on missing libraries.
  $form['libraries'] = array(
    '#type' => 'fieldset',
    '#title' => t('Presentation package'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($libraries),
  );

  $form['libraries']['table'] = array(
    '#value' => theme('table', array(), $libraries),
  );


  return system_settings_form($form);
}

/**
 * Retrieve presentation settings.
 * 
 * @param $setting
 *  The name of the variable (without any dynamic suffix)
 * @param $key
 *  If the setting is an array, it will retrieve the specified index value.
 * @param $plugin 
 *  The machine_name of the plugin for which to retrieve settings. If set, it will look for $setting_$plugin in the variables array.
 * @return
 *  Mixed. The stored value for the given setting.
 */
function presentation_get_setting($setting, $key = NULL, $plugin = NULL) {
  static $variables;

  if (empty($variables)) {
    $variables = _presentation_get_variables();
  }
  if ($plugin != NULL) {
    $setting .= '_' . $plugin;
  }
  if (is_array($variables[$setting]) && isset($key)) {
    return $variables[$setting][$key];
  }
  return $variables[$setting];
}

/**
 * Supplies default settings.
 */
function _presentation_get_variables() {
  $plugins = presentation_get_plugins();
  
  $vars = array();
  $vars['allowed_node_types'] = variable_get('presentation_allowed_node_types', array());

  $vars['default_arguments'] = variable_get('presentation_default_arguments', array(
    'title' => '[title]',
    'author' => '[author-name]',
    'email' => '[author-mail]',
    'organization' => '[site-name]',
    'location' => '',
    'date' => '[day], [mon] [date], [yyyy]',
  ));

  $vars['default_plugin'] = variable_get('presentation_default_plugin', 'handout');
  $vars['node_link_text'] = variable_get('presentation_node_link_text', t('View Presentation'));
  $vars['slide_source'] = variable_get('presentation_slide_source', array('type' => 'node', 'content' => '[node-body]'));
  
  return $vars;
}