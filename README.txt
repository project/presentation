# Presentation

The Presentation API allows plugins to export Node and (eventually) Views content through an external library or template file. 
This allows the use of external slideshow utilities such as Eric Meyer's [s5][] or Jordi Boggiano's [Slippy][] as 
plugins.

  1: http://meyerweb.com/eric/tools/s5/
  2: http://seld.be/notes/introducing-slippy-html-presentations

## I Want Books to Be S5 Presentations
If all you want is to dash a book up on the screen as an s5 Presentation, check out the [s5][] module.

  3: http://drupal.org/project/s5

## Types of Presentations
Presentation supports Nodes as presentation source content. In the future, Views will be supported.

### Node Presentations
Go to admin/settings/presentation to review your options. Different node types may be configured to be "presentable". 
Such nodes will have a link added to visit the Presentation version of their content.

There are several presentation fields that the Presentation module will handle for all plugins. They include:

* **Title:** Node title
* **Author:** Node author
* **Email:** Node author's email
* **Date:** Node post date
* **Location:** Blank  
* **Organization:** Site Name

This is all applied using the Token module. Advanced settings at admin/settings/presentation allow you to use any token you like for 
each value. This can be used to create dedicated CCK Fields for different elements of a presentation.

(In fact, you can designate the main body of the presentation by using tokens. By default the node body is used, but you could replace 
it with the contents of some other textarea.)

#### Writing Presentation Text
Presentations carve text up into sections based on HTML headings. The level of the heading is discarded, and the 
heading text itself is held apart as a title for the text.

### Views Presentations [Todo]
Goals for the Presentation module include leveraging any View as the basis for presentation slides. 
A presentation is still equated to a node, but the content itself will be drawn from Views--one 
slide per row.

This mechanism is what will power the Presentation Book feature.

## Shipped Features [In Progress]
Presentation comes with three example Features.

1. **Presentation Default**: This feature supplies a Presentation content type with fields for the Presentation Date, 
Location, and Presenters.
2. **Presentation Book**: N/A
3. **Presentation Atrium**: This feature duplicates Presentation Default and Presentation Book, but adds integration 
functionality for OpenAtrium. Extra functionality includes:
  * Spaces integration to turn Presentations on and off.
  * Enhanced s5 theming to use group color.
  * A View listing all presentations in the group.

## Todos

* **Views Filter - Is Presentable**: Restricts View to all Presentation Allowed Types. Hookable in 
some way so individual features using a Presentation View can specify other types of nodes (such as 
book parents) to be included.
* **Presentations per Content Type**: Instead of enabling Content Types to use the Presentation 
system, it would be better to set a default presentation plugin and enabled plugins per content 
type. (And View Presentation link?
