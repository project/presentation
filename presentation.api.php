<?php

/**
 * @file
 * Document the Presentation module API.
 */

/**
 * Define a Presentation plugin through which to render a presentation.
 *
 * @return
 * Returns an array with the following elements:
 *  'human name': [optional] Human readable name of the presentation engine. If missing, will be built from array index.
 *  'machine name': [optional] Machine readable name of the presentation engine. If missing, will be built from array index.
 *  'library': [optional] Various elements describing the plugin library. Used in Admin UI
 *    - 'homepage': URL of plugin homepage.
 *    - 'download': URL of plugin download page.
 *    - 'version': Which version of the library is recommended.
 *  'template path': Location to look for default export template. Default: modules/presentation/theme by default.
 *  'text process': Callback function that preprocesses text for presentation creation.
 *    Note: This is better handled by a theme preprocess function, but that would impede creating default preprocessing, which seems like a much more common use-case.
 *    Current text processors shipped with Presentation include:
 *    - presentation_text_slides_by_heading_process: (default) The largest heading level in the text will be computed and used to separated text into slides. The actual heading will be the slide title. Text before the first heading will be discarded from the presentation.
 *    - presentation_text_raw_process: The entire text will be passed on to the presentation template unaltered.
 * @see presentation_get_plugins, presentation_text_slides_by_heading_process, presentation_text_raw_process
 */
function hook_presentation_plugin_info() {
  $plugins = array();

  $plugins['handy slideshow'] = array(
    'human name' => 'Handy Slideshow Example',
    'machine name' => 'handy_slideshow',
    'template path' => drupal_get_path('module', 'handy_show') . '/theme'
  );

  return $plugins;
}

/**
 * Allows other modules to alter presentation plugin definitions.
 */
function hook_presentation_plugin_info_alter(&$plugins) {
  foreach ($plugins as &$plugin) {
    $plugin['template path'] = drupal_get_path('module', 'mymodule') . '/theme';
  }
}