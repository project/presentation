<?php
/**
 * This template is used to print a single field in a view. It is not
 * actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the
 * template is perfectly okay.
 *
 * Variables available:
 * - $plugin: The presentation plugin definition array.
 * - $node: Node object.
 * - $slides: Individual slides of the show. array('title' => text)
 * - $arguments: Associative array of values that may be injected into the template. Assumes template knows what they are.
 */
?>

<?php extract($arguments); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>" xml:lang="<?php print $language; ?>" dir="<?php print $language_rtl ? 'rtl' : 'ltr'; ?>">
<head>
  <?php print drupal_get_html_head(); ?>
  <title><?php print $title; ?></title>
  <base href="<?php print $base_url; ?>" />
  <meta name="generator" content="<?php print $plugin['label']; ?> Presentation" />
  <meta name="version" content="Handout 1.0" />
  <meta name="presdate" content="<?php print $date; ?>" />
  <meta name="author" content="<?php print $author; ?>" />
  <meta name="defaultView" content="slideshow" />
  <meta name="controlVis" content="hidden" />
  <link type="text/css" rel="stylesheet" href="misc/print.css" />
  <?php if ($language_rtl): ?>
    <link type="text/css" rel="stylesheet" href="misc/print-rtl.css" />
  <?php endif; ?>
</head>
<body>
  <div class="header">
    <?php print $author; ?>
    <?php if (!empty($email)) { print '&middot; ' . $email; } ?>
    <?php if (!empty($path)) { print '&middot; ' . $path; } ?>
  </div>
  <hr />
  <h1><?php print $title; ?></h1>
  <div class="presentation">
    <?php foreach ($slides as $slide) { print $slide; } ?>
  </div>
  <?php if (!empty($footer)):?>
  <hr />
  <div class="footer"><?php print $footer; ?></div>
  <?php endif; ?>
</body>
</html>