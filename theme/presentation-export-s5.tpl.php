<?php
/**
 * This template is used to print a single field in a view. It is not
 * actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the
 * template is perfectly okay.
 *
 * Variables available:
 * - $plugin: The presentation plugin definition array.
 * - $slides: Individual slides of the show. array('title' => text)
 * - $arguments: Associative array of values that may be injected into the template. Assumes template knows what they are.
 * - $inline_styles: CSS for inline injection.
 */
?>
<?php extract($arguments); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php print $title; ?></title>
  <base href="<?php print $base_url; ?>" />
  <?php print drupal_get_html_head(); ?>
  <!-- metadata -->
  <meta name="generator" content="<?php print $plugin['label']; ?>" />
  <meta name="version" content="S5 1.1" />
  <meta name="presdate" content="<?php print $date; ?>" />
  <meta name="author" content="<?php print $author; ?>" />
  <meta name="company" content="<?php print $organization; ?>" />
  <!-- configuration parameters -->
  <meta name="defaultView" content="slideshow" />
  <meta name="controlVis" content="hidden" />
  <!-- style sheet links -->
  <?php
    // Support external themes. Themes added to the s5 settings form that are full paths to a theme directory will be included as is.
    $theme_path = $plugin['library'] . '/' . $plugin['options']['theme'];
  ?>
  <link rel="stylesheet" href="<?php print $theme_path; ?>/slides.css" type="text/css" media="projection" id="slideProj" />
  <link rel="stylesheet" href="<?php print $plugin['library']; ?>/default/outline.css" type="text/css" media="screen" id="outlineStyle" />
  <link rel="stylesheet" href="<?php print $plugin['library']; ?>/default/print.css" type="text/css" media="print" id="slidePrint" />
  <link rel="stylesheet" href="<?php print $plugin['library']; ?>/default/opera.css" type="text/css" media="projection" id="operaFix" />
  <?php if (!empty($inline_styles)): ?>
  <style type="text/css">
  <?php print $inline_styles; ?>
  </style>
  <?php endif; ?>
   <!-- S5 JS --> 
  <script src="<?php print $plugin['library']; ?>/default/slides.js" type="text/javascript"></script> 
</head>
<body>
  <div class="layout">
  <div id="controls"></div>
  <div id="currentSlide"></div>
  <div id="header"></div>
  <div id="footer">
    <h1><?php print $location; ?></h1>
    <h2><?php print $title . ' &middot; ' . $date . ' [' . $link . ']'; ?></h2>
  </div>
  </div>
  <div class="presentation">
    <div class="slide">
    <h1><?php print $title; ?></h1>
    <?php if(!empty($author)): ?>
    <h3><?php print $author; ?></h3>
    <?php endif; ?>
    <?php if(!empty($organization)): ?>
    <h4><?php print $organization; ?></h4>
    <?php endif; ?>

    <?php foreach ($slides as $slide) { ?>
    <div class="slide">
    <h1><?php print $slide['title']; ?></h1>
    <?php print $slide['text']; ?>
    </div>
    <?php } ?>
  </div>
</body>
</html>
