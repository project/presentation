<?php

/**
 * @file
 *  Provide core display plugins for presentation module.
 */ 

/**
 * Return default "handout" plugin.
 * The handout plugin is very basic and relies on default processing for all behaviors.
 * You may override theme/presentation-handout-export.tpl.php to change it's appearance.
 */
function presentation_presentation_plugin_info() {
  $plugins = array();
  $plugins['handout'] = array(
    'process' => 'presentation_text_raw_process',
    'library' => PRESENTATION_SIMPLE_PLUGIN,
  );
  $plugins['s5'] = array(
    'label' => t('s5 Presentation'),
    'process' => 'presentation_text_slides_by_heading_process',
    'settings' => 'presentation_s5_settings',
    'options' => variable_get('presentation_s5_settings', array('theme' => 'default')),
    'package' => array(
      'version' => '1.1',
      'homepage' => 'http://meyerweb.com/eric/tools/s5/',
      'help' => t('Unzip the s5 package and move the contents of the ui/ directory to libraries/s5/.'),
    ),
  );
  return $plugins;
}

/**
 * Settings callback for s5 plugin.
 */
function presentation_s5_settings($plugin) {
  $form = array();
  $themes = array();
  foreach (scandir($plugin['library']) as $handle) {
    if (is_dir($plugin['library'] . '/' . $handle) && $handle != '.' && $handle != '..') {
      $theme = basename($handle);
      $themes[$theme] = $theme;
    }
  }
  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('S5 Theme'),
    '#default_value' => $plugin['options']['theme'],
    '#options' => $themes,
    '#description' => t('Select a theme.')
  );
  return $form;
}

function presentation_preprocess_presentation_export_s5(&$vars) {
  if (empty($vars['inline_styles'])) {
    $vars['inline_styles'] = '';
  }
  $vars['inline_styles'] .= 'h2>:link, h2>:visited {color: #99a; font-size:80%;}';
}